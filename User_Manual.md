# Troglometer <br/> User Manual

**An Open-Source Hardware Cave Monitoring System[^1]**

Version 2.0

[^1]:The Troglometer was developed by the
CommonsLab.gr MAKERSPACE Koin.S.Ep.
in collaboration with and on behalf of the 
Hellenic Institute of Speleological Research (HISR / INSPEE)
as part of the
GRECABAT project,
co-funded by the
European Union
under the LIFE Programme (LIFE17 NAT/GR/000522)
and the
Green Fund
and supported by 
A. G. Leventis Foundation 
and 
Stavros Niarchos Foundation. 

## Introduction

The Troglometer is a set of battery-powered environment sensors, 
connected to data-logging devices, which are optimized for use 
in underground and high-humidity environments, such as caves, canyons or tunnels.
There are three main data-logging modules, which make up the Troglometer:

1. **TrogloCam**  
   Records images with an infra-red sensitive camera, 
   illuminated with an infrared spotlight.
2. **TrogloWater**  
   Records conductivity and temperature in water reservoirs or water streams.
3. **TrogloAir**  
   Records temperature and relative humidity of the air, 
   wind strength and direction, as well as moisture in the soil 
   and brightness of the ambient light.

Additionally, there are two optional adapter modules as add-on:[^2]

1. **TrogloNet**  
   Connects up to three Troglometer modules to the Internet 
   through an Ethernet adapter.
2. **TrogloPort**  
   Connects a Troglometer module to a PC though a USB connection.

[^2]: At the time of writing, the optional add-on modules are still under development and only available as test-systems.

## Usage

### Getting Started

In order to activate any Troglometer module, you need open the case of the 
Troglometer module. 

#### Preparing the microSD flash memory card
For any Troglometer module to function correctly, 
you need a microSD card of 4, 8 or 16 GB, 
which has been formatted with the FAT16 or FAT32 file system. 
Most microSD cards on the market come pre-formatted with one of the above file systems.

Additionally, you may create a text file on the microSD to configure the
interval in which the Troglometer module records data.
To achieve this, create or edit a file named `INTERVAL.TXT`, 
which has to be placed directly in the root of the file system 
and not inside any folder or sub-folder.
The file should contain one single line with one single integer number (e.g. `15`)
which corresponds to the number of minutes, 
that the Troglometer module waits between each set of measurements.

Insert the microSD card into the card-reader slot, 
which is located on the system board inside the case. 

#### Preparing the batteries
Insert three batteries of size AA (also known as *penlite*, *Mignon*, HR6, or 15H) 
into the battery holder and connect the battery holder to the system board, 
using with the clip-on connector. 
We recommend using rechargeable batteries with low self-discharge 
nickel metal hydride (LSD NiMH) technology, such as those which are marketed as 
“pre-charged” or “ready to use”, 
but also alkaline or zinc-carbon AA batteries can be used.
Even nickel-cadmium AA batteries can be used, 
but they are likely to last a much shorter, 
due to their inherent self-discharge.

#### Starting the system
When the microSD card has been inserted and the module is powered by the batteries, 
press the "ON" button for about three seconds
for the Troglometer to power up and enter the boot-up sequence. 
During boot-up, for ten seconds, the device will flash the on-board status LED
**once every second**, while it is waiting for communication through the `COM+CHARGE` port.
During this time it is possible to initiate communication with the Troglometer module, 
using the TrogloPort module connected to the USB port of a PC, which runs a serial terminal program[^3]. 
(See section *Boot Menu*.)

In case the user enters the boot menu, the on-board LED will flash 
**twice every second** for 30 seconds, waiting for the user to enter a command.
If the user does not enter the boot menu or after device exits the boot menu, 
the device will begin to record data from the sensors. 
From that time on, the on-board LED will flash **once every 10 seconds** while in data-logging mode, indicating normal operation.

[^3]: There are many serial terminal programs available for various operating
systems (GNU/Linux, MS Windows, Apple MacOS, etc.) 
The following are some open-source free software programs, which can be used 
to communicate with a Troglometer module using the TrogloPort 
connected to the USB port of a PC: [Arduino IDE](https://www.arduino.cc/en/Main/Software#download), [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/)

Unless defined differently, the TrogloAir and TrogloWater modules 
record values from the connected sensors every 10 minutes 
and store them in a log file, which is saved on the microSD card 
with the file name `DATALOG.TXT`. 
The TrogloCam module, by default, takes one photo every 1 hour, 
which are also stored on the microSD card in JPEG format. 
These intervals can be changed either through the boot menu 
or through the `INTERVAL.TXT` file.

#### Closing the case
After the devices are prepared, powered and activated, 
you should close the case in order to protect the electronics 
from the harsh environment where they may be placed. 
When closing the case, make sure that the white rubber gasket, 
which is used to seal the lid on the case and protect from dust and humidity, 
is not damaged and is properly placed inside the channel, 
which runs around the perimeter of the lid.


### Replacing the batteries
We recommend to remove the batteries when the Troglometer is not being used. 
To remove the batteries from the Troglometer, simply open the case and remove 
the batteries one-by-one from the battery holder.

We suggest to use a high-quality battery charging device, 
which can charge and monitor every AA battery cell individually, 
in order to optimally charge every battery cell, 
according to it's respective discharge level and capacity. 
However, for an occasional top-up of the battery charge, 
it is possible to charge the battery pack through the COM+CHARGE port.

How long a set of batteries will be able to power a Troglometer module depends 
on a number of factors, such as the capacity of the batteries, the age and 
quality of the batteries, how often measurements are taken and on the ambient 
temperature. Indicatively, a set of new, fully charged 2500mAh LSD NiMH
batteries in an environment below 15°C can be expected to take measurements 
every 10 minutes (or photos every 60 minutes) for about 3 months or more.

In case that even longer battery run-times are required, it is possible to add 
external batteries and connect them to the system through the COM+CHARGE port.


### Retrieving the Data
After the Troglometer has collected data for a desired period of time, 
there are two ways that the data can be retrieved:
1. Open the case, remove the batteries, 
   and remove the microSD card from the on-board microSD card-reader. 
   We recommend to do this in a safe environment, 
   with moderate temperature and humidity levels.
   The microSD card will have a file named `DATALOG.TXT`. 
2. The `DATALOG.TXT` file can also be downloaded through the boot menu, 
   using a TrogloPort module connected to the COM+CHARGE port.
   (See section *Boot Menu*.)

This `DATALOG.TXT` file contains all the data measured from all the sensors 
in a simple format: 

Every line corresponds to one measurement event. 
The date and time of the measurement event are the first entries on every line. 
Every other entry in that line corresponds to the value 
measured from one specific sensor. 
The entries in every line are separated by a tabulation character (U+0008). 

Such lists of data can easily be imported into various data-analysis or 
spreadsheet programs, such as any of the following:

* LibreOffice Calc 
* Microsoft Excel
* MatPlotLib
* GNU Octave
* MATLAB

Using any of the above programs, it is possible to analyze and 
visualize the data according to your preference.


### Boot Menu
The user can communicate with the Troglometer module, 
through the UART[^4] of the COM+CHARGE port, 
e.g. using the TrogloPort connected to a PC, running a serial terminal program. 

[^4]: UART: Universal Asynchronous Receiver-Transmitter

After power-up, or after a reset, 
which can be triggered through the TrogloPort, 
the Troglometer sends a message through the UART 
and waits for 10 seconds for communication with the user, 
flashing the status LED once per second.

To activate the boot menu, the user needs to send `[ENTER]`, 
i.e. a carriage-return (ASCII 0x0A) or a line-feed (ASCII 0x0D).
To confirm that the user has entered the boot menu, 
the Troglometer prints out the following options and
the LED flashes twice every second.

Inside the boot menu, the user can choose any the following options 
by sending the respective letter:

- **p: Print log**  
  The Troglometer will print the contents of the `DATALOG.TXT` file.

- **d: Delete log**  
  This will delete the `DATALOG.TXT` file from the microSD card.

- **t: Set time**  
  The user can set the clock (the time and the date) of the device.

- **i: Set interval**  
  The user can define the number of minutes that the Troglometer will
  wait between subsequent collection of data from the sensors. 
  This value will be saved and will be the new default interval value, 
  ever after the device has been without battery power for some time.

- **q: Quit menu**  
  Leave the boot menu and start measuring.



## Specifications

Each of the TrogloAir, TrogloWater and TrogloCam modules host the following peripherals:

- Three modular connector sockets of type 8p8c (*"JR45"*) for connection 
  of the off-board sensors, IR-flash and communication.
- Highly accurate real-time clock module based on 
  the *Maxim Integrated* DS3132 timer chip,[^5] 
  which has the following characteristics:
    * Accuracy ±2ppm (1 minute per year) from 0°C to +40°C
    * Automatic leap-year correction.
    * On-board 2032-type time-keeping back-up battery holder.
    * On-board 4KiB non-volatile data memory
- MicroSD card holder
- Status LED
- Battery pack connector
- Fuse holder (use 2A fast-blow)
- Start button
- In-box air temperature/humidity/pressure sensor (optional)
- Ambient light sensor (TrogloAir only)
- 12-Volt power supply for LED spotlight (TrogloCam only)

[^5]:
[https://datasheets.maximintegrated.com/en/ds/DS3231.pdf]()
[https://protosupplies.com/wp-content/uploads/2018/10/DS3231-with-EEPROM-Schematic-2.jpg]()


### TrogloAir

The TrogloAir module can measure the following parameters:

* Air temperature in degrees Celsius
* Relative air humidity in percent
* Air pressure in Pascal
* Wind speed in m/s
* Wind direction in degrees
* Ground humidity in percent
* Ground temperature in degrees Celsius
* Luminosity in Lux


#### Combined Air Sensor
Three parameters of the air (temperature, relative humidity, and pressure)
are measured by one single sensor module, which is based on the 
*Bosch Sensortec* BME280 sensor.[^6]

[^6]: [https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme280-ds002.pdf]()

Two of these 3-in-1 sensors are used for the TrogloAir module, 
one is placed inside the Stevenson shield in order to measure ambient conditions, 
whereas the other sensor is placed inside the case to monitor the 
temperature and humidity that the main electronics are exposed to.

#### Wind meters
The wind speed meter is based on the hemispherical cup design, 
which typically have a non-linearity error of approximately 2%-5%.
A magnetic reed switch translates the rotation of the cup anemometer
to electric pulses, which get read by the micro-controller of the 
Troglometer module.

The wind direction detector is a simple wind vane. 
A set of eight magnetic reed switches 
detects the direction where the wind vane is pointing at, 
so that the precision is in steps of 45 degrees,
i.e. N, NE, E, SE, S, SW, W, NW. 


#### Ground Moisture
The water content in the ground is estimated 
by measuring the electrical capacitance of the ground.
This gives an approximation, which is expressed in percent, 
where 0% corresponds to completely dry sand and 100% corresponds to water.


#### Ground Temperature
The temperature of the ground is measured using a waterproof probe, 
fitted with a *Maxim Integrated* DS18B20 sensor,[^7] 
with the following characteristics:

* ±0.5°C Accuracy from -10°C to +85°C
* -55°C to +125°C operating range
* 0.0625°C resolution

[^7]:
[https://web.archive.org/web/20190608084659/https://www.maximintegrated.com/en/products/sensors/DS18B20.html]()
[https://web.archive.org/web/20190608084659/https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf]()

#### Luminosity
The very-high sensitivity ambient light sensor TSL2591[^8] by AMS AG 
can sense even very low ambient light down to milli-Lux levels, 
such as a moonless night in the countryside.

[^8]:
[https://ams.com/TSL25911]()
[https://ams.com/documents/20143/36005/TSL2591_DS000338_6-00.pdf]()

### TrogloWater

The TrogloWater module can measure the following parameters:

* Air temperature in degrees Celsius
* Relative air humidity in percent
* Air pressure in Pascal
* Water conductivity in mS/cm
* Ground temperature in degrees Celsius

#### Combined Air Sensor
Three parameters of the air (temperature, relative humidity, and pressure)
are measured by one single sensor module, which is based on the 
*Bosch Sensortec* BME280 sensor.[^6]

One of these 3-in-1 sensors is placed inside the case 
of the TrogloWater module to monitor the 
temperature and humidity that the main electronics are exposed to.

#### Ground Temperature
The temperature of the ground is measured using a waterproof probe, 
fitted with a *Maxim Integrated* DS18B20 sensor[^7] 
with the following characteristics:

* ±0.5°C Accuracy from -10°C to +85°C
* -55°C to +125°C operating range
* 0.0625°C resolution

#### Water conductivity
To measure the conductivity of water reservoirs or streams, 
a parallel-plate probe in connection with a small-signal amplifier is used.
Probes are available with different probe constants: 

1. K=0.1 (approx. 0.05- 0.2 mS/cm)
2. K=1.0 (approx. 0.5 - 20  mS/cm)
3. K=10  (approx. 10  - 100 mS/cm)



### TrogloCam
The camera module hosts an *OmniVision* 5 MegaPixel camera sensor[^9]
on an ArduCam 5MP plus module[^10] with the following specifications:

* Maximum image resolution: 2592 pixel x 1944 pixel
* Image sensor area: 3.7 mm x 2.7 mm
* Image format: 10-bit RAW RGB, BMP, JPEG

[^9]:
[https://cdn.sparkfun.com/datasheets/Sensors/LightImaging/OV5640_datasheet.pdf]()
[https://media.digikey.com/pdf/Data%20Sheets/OmniVision%20PDFs/OV5642.pdf]()

[^10]:
[https://www.arducam.com/product/arducam-5mp-plus-spi-cam-arduino-ov5642/]()

## Maintenance

### Cleaning
If required, it is possible to clean outside of the the cases of the Troglometer modules with a damp soft cloth and a mild soap to remove dust and dirt. 
The transparent lid of the cases can be removed and cleaned from the inside in the same way.
Also the wind meters, 
the Stevenson shield,
the metallic temperature probes, 
the soil moisture sensor, 
the infra-red spotlight 
and the cables can be cleaned in the same way.

The bottom part of the cases, which hosts the electronics of the Troglometer, can be blown with clean air to remove dust, if necessary.

The conductivity sensor can be rinsed with de-ionized water.


### Replacement of Batteries
The Troglometer modules use a battery pack of 
three AA batteries as a main power supply 
and a 3-Volt coin cell battery 
as backup for the internal clock while changing the main battery pack.

The main battery pack can be filled with either rechargable batteries, 
such as nickel metal hydride (NiMH) or nickel-cadmium (NiCd) cells, 
or with non-rechargable cells, such as alkaline or zinc-carbon cells.
We recommend low self-discharge NiMH cells for the main battery pack.

The Troglometer modules come fitted with a non-rechargable CR2032 coin cell, which will last for years.
The coin cell battery for the clock must be of `2032` size, i.e. 20mm diameter, 3.2mm thickness. 
There are also rechargable options available, such as ML2032 or LIR2032.
In order to replace the coin cell battery of the clock module, 
gently press the small metal latch towards the outside to release the coin cell.

### Replacement of Sensors
The sensor elements of the Troglometer modules can be considered consumables, 
and some sensors may need to be replaced after months of continuous usage.
That said, most sensors are made to be resilient to the harsh conditions, 
which are found inside of the underground environments, 
and they might last for many years.

The water conductivity sensor, may need cleaning, re-calibration or replacement 
when exposed to high-conductivity, highly acidic or corrosive waters for long periods of time.

Some sensors, especially the relative air humidity sensor, 
may saturate when exposed to condensation of water from the ambient air, 
or when exposed to near 100% relative humidity for extended periods of time.
In that case, the sensor may show a too high value for the relative humidity 
until the sensor has relaxed to normal values. 
The relaxation can be accelerated by following drying the sensor 
according to the procedure laid out in the data sheet of the BME280 sensor. 

## Source Code
The firmware of the Troglometer modules is programmed in ARDUINO-flavoured C++ code.
The source code is released as open-source free software, 
available under the terms of the GNU AGPL 3.0 licence.
The source code can be found on-line in the following git repository:

[https://gitlab.com/commonslabgr/troglometer/firmware]()

## Design Files
All design files are open-access and freely available freely 
under the terms of the Creative Commons Attribution-ShareAlike 4.0 International License.[^11]
The git repository for storing the design files can be found on-line at the following address:

[https://gitlab.com/commonslabgr/troglometer/hardware]()

[^11]:[http://creativecommons.org/licenses/by-sa/4.0/.]()


#### END NOTES: